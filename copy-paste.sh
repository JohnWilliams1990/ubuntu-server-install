#!/bin/bash

if [ "$(whoami)" != 'root' ]; then
  printf "\n\tMust be run as root or with sudo/dzdo\n"
  exit
fi

wget https://askubuntu.com/questions/22743/how-do-i-install-guest-additions-in-a-virtualbox-vm
#####################
# https://askubuntu.com/questions/22743/how-do-i-install-guest-additions-in-a-virtualbox-vm
# echo y | apt-get install virtualbox-guest-additions-iso
# echo y | apt install build-essential dkms
#############################
mkdir cddrive
mount /usr/share/virtualbox/VBoxGuestAdditions.iso cddrive
# sh VBoxLinuxAdditions.run
$(pwd)/cddrive/VBoxLinuxAdditions.run
umount cddrive
rmdir cddrive

printf "\n\n\tPlease run the following as root:"
printf "\n\t1. $(pwd)/cddrive/VBoxLinuxAdditions.run"
printf "\n\t2. Shutdown machine and enable bidirectional copy-paste in virtualbox."
printf "\n\t3. restart machine.
"

